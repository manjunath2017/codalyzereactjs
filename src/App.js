import React, {useReducer}  from 'react';
import './App.css';
import EditProducts from './component/edit-product';
import Products from './component/products';
import PageNotFound from './component/pageNotFound';
import Nav from './Nav';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ProductsJson from './products.json';

export const countContext=React.createContext();
const initialState = {
  Data:ProductsJson,
}
const reducerFn = (curresntState, action)=>{
  switch(action.type){
    case 'UPDATE':
        return {
            Data:action.payload,
        }
    default:
        return curresntState    
}
}
const App=()=>{
const [data, dispatch] = useReducer(reducerFn, initialState);



     return(
      <Router>
      <Nav />
      <countContext.Provider value={{jsonData:data, Dispatch:dispatch}}>
        <Switch>
            <Route path="/edit-product" exact component={EditProducts} />
            <Route path="/" exact component={Products} />
            <Route component={PageNotFound} />
            </Switch>
        </countContext.Provider>
      </Router>
     )
 } 
 
export default App;