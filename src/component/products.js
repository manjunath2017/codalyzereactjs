import React, { Fragment, useContext } from 'react';
import {Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { countContext } from '../App';

const Products = () => {
    const ContextObj = useContext(countContext);
    const jsonProducts = ContextObj.jsonData.Data.product;
    console.log(jsonProducts);
    return (
        <Fragment>
            <div className="container">
                <h2>Products</h2>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Weight</th>
                            <th>Availability</th>
                            <th>isEditable</th>
                        </tr>
                    </thead>
                    <tbody>
                {
                    jsonProducts.map((data, index)=>(
                        <tr key={index}>        
                            <td>{data.name}</td>
                            <td>{data.weight}</td>
                            <td>{data.availability}</td>
                            <td>{ data.isEditable? <Link  to={{ pathname:"/edit-product", editData:{data}}}   className="btn btn-primary btn-sm"  >  <i className="fa fa-edit"> Edit </i> </Link>:'' }</td>
                        </tr>
                        )
                    )
                }
                    </tbody>
                </table>
            </div>
        </Fragment>
    )
}
export default Products;