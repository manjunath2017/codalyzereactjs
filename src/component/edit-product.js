import React, { Fragment, useContext, useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { countContext } from '../App';


const EditProducts = props => {
    // var errorObj=;
    var [error, setError] = useState({
        errorName: "",
        errorWeight: "",
        errorProductUrl: "",
        errorPriseTire: "",
    });
    // console.log(error.errorWeight, '-----');


    var data = props.location.editData.data;
    var [getSubmit, setSubmit] = useState(false);
    var { name, weight, availability, productUrl, pricingTier, isEditable, priceRange } = data;

    
    // console.log(data, 'data');
    // console.log(name, weight, availability, productUrl, pricingTier, isEditable, '---------');

    const ContextObj = useContext(countContext);
    const inputTextHandler = event => {
        event.preventDefault();
        // console.log('weight');

        // console.log(event.target.value, '...ContextObj.data');
        var { name, value } = event, target;
        ContextObj.Dispatch(...data,{[name]: value}); // get all input values

        if (toString(name) === "") {
            setError({ errorName: "Name field required!" });
        }
        if (toString(weight)  === "") {
            setError({ errorWeight: "Weight field required!" });
        }
        if (toString(productUrl)  === "") {
            setError({ errorProductUrl: "ProductUrl field required!" });
        }
        if(toString(name) === "" ||toString(weight)  === "" || toString(productUrl)  === ""){
            setSubmit(true);
        }
    }

    var [radio, setRadio] = useState(pricingTier);
    const radioButton = (a) => {
        if (a === 'premier')
            return setRadio(1);
        setRadio(0);
    }
    var [isEditableChange, setIsEditableChange] = useState(isEditable);
    const redirectToIndex=()=>{
        console.log('true--- redirect');
        props.history.push("/");
    }
    return (
        <Fragment>
            <div className="container">
                <h1>Hello Edit Products </h1>
                <form>
                    {/* <div className="form-group">
                    <label htmlFor="uname">name</label>
                    {ContextObj.jsonData.Data.name+ "------"}
                    <input type="text" className="form-control" onChange={()=> ContextObj.Dispatch({type:'UPDATE_Name', payload:name})} placeholder="Enter name" name="name" value={name}  />
                </div> */}

                    <div className="form-group">
                        <label htmlFor="uname">name</label>
                        <input type="text" className="form-control" onChange={inputTextHandler} placeholder="Enter name" name="name" value={name} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="uname">Weight </label>
                        <input type="text" className="form-control" onChange={inputTextHandler} placeholder="Enter Weight" name="weight" value={weight} />
                        <label style={{ color: "#f00" }}>{error.errorWeight ? error.errorWeight + '-----' : ''}</label>
                    </div>
                    <div className="form-group">
                        <label htmlFor="uname">Availability</label>
                        <input type="number" className="form-control" onChange={() => inputTextHandler} placeholder="Enter Availability" name="availability" value={availability} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="uname">Product Url</label>
                        <input type="text" className="form-control" onChange={() => inputTextHandler} placeholder="Enter Url" name="url" value={productUrl} />
                        <label style={{ color: "#f00" }}>{error.errorProductUrl ? error.errorProductUrl + '-----' : ''}</label>
                    </div>
                    <label>pricingTier : </label>
                    <div className="radio-inline">
                        <label> <input type="radio" name="pricingTier" onChange={() => inputTextHandler} checked={pricingTier === 'budget' ? 'budget' : ''} value={pricingTier} onClick={() => radioButton('budget')} /> Budget</label>
                    </div>
                    <div className="radio-inline">
                        <label> <input type="radio" name="pricingTier" onChange={() => inputTextHandler} checked={pricingTier === 'premier' ? 'premier' : ''} value={pricingTier} onClick={() => radioButton('premier')} /> Premier</label>
                    </div>
                    <div className="form-group">
                        <select className="form-control" onChange={() => inputTextHandler}>
                            {pricingTier === 'budget' ? `<option> ${pricingTier}</option>` : ``}
                            {pricingTier === 'premier' ? `<option> ${pricingTier}</option>` : ``}
                            <option value={priceRange}>{radio === 0 ? '4k - 6k' : '11k - 20k'}</option>
                            <option value={radio === 0 ? '4k - 6k' : '11k - 20k'}>{radio === 0 ? '4k - 6k' : '11k - 20k'}</option>
                            <option value={radio === 0 ? '6k - 9k' : '21k - 30k'}>{radio === 0 ? '6k - 9k' : '21k - 30k'}</option>
                            <option value={radio === 0 ? '9k - 11k' : '30k+'}>{radio === 0 ? '9k - 11k' : '30k+'}</option>
                        </select>
                    </div>
                    <div className="radio-inline">
                        <label className="checkbox-inline"><input type="checkbox" value={isEditableChange} onChange={() => inputTextHandler} />is Editable</label>
                    </div>
                    <div className="form-group">
                        <button type="button" disabled={getSubmit} onClick={redirectToIndex} className="btn btn-primary" >Submit</button>
                    </div>
                </form>
            </div>
        </Fragment>
    )
}
export default EditProducts;
